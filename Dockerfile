FROM registry.gitlab.com/janpoboril/drupal-composer-docker:7.2-apache

# For server build without code in volume
ADD drupal /var/www/drupal
RUN composer install
